package com.lichen.utility;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lichen.db.City;

public class JsonUtil {

	public static City parseJson(String jsonString) {
		City city = City.getInstance();
		try {
			JSONObject jsonObject = new JSONObject(jsonString);
			JSONObject weatherInfo = (JSONObject) jsonObject.get("weatherinfo");
			String cityId = weatherInfo.getString("cityid");
			String cityName = weatherInfo.getString("city");
			String lowTemp = weatherInfo.getString("temp1");
			String highTemp = weatherInfo.getString("temp2");
			String weather = weatherInfo.getString("weather");
			String pTime = weatherInfo.getString("ptime");
			
			city.setCityId(Long.parseLong(cityId));
			city.setCityName(cityName);
			city.setHighTemp(highTemp);
			city.setLowTemp(lowTemp);
			city.setWeather(weather);
			city.setpTime(pTime);
		} catch (JSONException e) {
			Log.e("wisper", "����jsonʧ��");
		}
		return city;
	}
}
