package com.lichen.utility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class HttpUtil {

	public static String getWeatherData(String urlString) {
		String data = new String();
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();

			connection.setReadTimeout(60 * 1000);
			connection.setConnectTimeout(60 * 1000);
			connection.setRequestMethod("GET");
			connection.connect();

			int responseCode = connection.getResponseCode();

			InputStreamReader streamReader = new InputStreamReader(
					connection.getInputStream(), "UTF-8");
			BufferedReader bufferReader = new BufferedReader(streamReader);
			data = bufferReader.readLine().toString();
			streamReader.close();
			bufferReader.close();
		} catch (Exception e) {
			Log.e("wisper", "获取中国国家气象局的天气数据失败");
		}
		return data;
	}

	public static boolean hasNetWork(Context context) {
		ConnectivityManager connManger = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connManger.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected())
			return true;
		return false;
	}
}
