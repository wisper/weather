package com.lichen.utility;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.lichen.db.City;
import com.lichen.db.CityContentProvider;

public class PrepareData {

	private Context mContext;
	
	public PrepareData(Context context) {
		mContext = context;
	}
	
	public boolean isAlreadyInitData() {
		Cursor cursor = mContext.getContentResolver().query(City.CONTENT_URI, null, null, null, null);
		if (cursor != null && cursor.moveToNext()) {
			return true;
		}
		return false;
	}
	
	public void initData() {
		try {
			ArrayList<ContentProviderOperation> op = new ArrayList<ContentProviderOperation>();
			ContentValues cv = new ContentValues();
			
			cv.put(City.CITY_ID, 101010100);
			cv.put(City.CITY_NAME, "北京");
			cv.put(City.CITY_NICKNAME, "bj");
			cv.put(City.CITY_DESCRIBE, "北京");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101020100);
			cv.put(City.CITY_NAME, "上海");
			cv.put(City.CITY_NICKNAME, "sh");
			cv.put(City.CITY_DESCRIBE, "上海");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101030100);
			cv.put(City.CITY_NAME, "天津");
			cv.put(City.CITY_NICKNAME, "tj");
			cv.put(City.CITY_DESCRIBE, "天津");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101090101);
			cv.put(City.CITY_NAME, "石家庄");
			cv.put(City.CITY_NICKNAME, "sjz");
			cv.put(City.CITY_DESCRIBE, "石家庄");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			
			cv.put(City.CITY_ID, 101080101);
			cv.put(City.CITY_NAME, "呼和浩特");
			cv.put(City.CITY_NICKNAME, "hhht");
			cv.put(City.CITY_DESCRIBE, "呼和浩特");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101100101);
			cv.put(City.CITY_NAME, "太原");
			cv.put(City.CITY_NICKNAME, "ty");
			cv.put(City.CITY_DESCRIBE, "太原");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101070101);
			cv.put(City.CITY_NAME, "沈阳");
			cv.put(City.CITY_NICKNAME, "sy");
			cv.put(City.CITY_DESCRIBE, "沈阳");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101070201);
			cv.put(City.CITY_NAME, "大连");
			cv.put(City.CITY_NICKNAME, "dl");
			cv.put(City.CITY_DESCRIBE, "大连");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101060101);
			cv.put(City.CITY_NAME, "长春");
			cv.put(City.CITY_NICKNAME, "cc");
			cv.put(City.CITY_DESCRIBE, "长春");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101050101);
			cv.put(City.CITY_NAME, "哈尔滨");
			cv.put(City.CITY_NICKNAME, "heb");
			cv.put(City.CITY_DESCRIBE, "哈尔滨");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			cv.put(City.CITY_ID, 101190101);
			cv.put(City.CITY_NAME, "南京");
			cv.put(City.CITY_NICKNAME, "nj");
			cv.put(City.CITY_DESCRIBE, "南京");
			op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
			
			mContext.getContentResolver().applyBatch(CityContentProvider.AUTHORITY, op);
		} catch (Exception e) {
			Log.e("CityDatabaseHelper", e.getMessage());
			e.printStackTrace();
		}
	}
}
