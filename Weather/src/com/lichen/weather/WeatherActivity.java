package com.lichen.weather;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.lichen.db.City;
import com.lichen.db.CityContentProvider;
import com.lichen.utility.HttpUtil;
import com.lichen.utility.JsonUtil;
import com.lichen.utility.PrepareData;

public class WeatherActivity extends Activity implements OnClickListener {

	private TextView title;
	private EditText cityInput;
	private Button btnSearch;
	private ProgressDialog dialog;
	private ProgressDialog mInitDialog;

	/* 抽屉导航变量 */
	private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    /* 定义常量 */
    private static final int MESSAGE_PARSE_DATA = 1;
    private static final int MESSAGE_DRAWER_ITEM = 2;
    private static final int MESSAGE_SPINNER_ITEM = 3;

    /* action_bar */
    private Spinner mActionbarSpinne;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_weather);
		/*显示App icon左侧的back键*/
		ActionBar actionBar = getActionBar();
	    actionBar.setDisplayHomeAsUpEnabled(true);

		title = (TextView) findViewById(R.id.title);
		cityInput = (EditText) findViewById(R.id.city_input);
		btnSearch = (Button) findViewById(R.id.btn_search);
		btnSearch.setOnClickListener(this);
		//准备插入数据的进度条
		mInitDialog = new ProgressDialog(this);
		mInitDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mInitDialog.setMessage(getString(R.string.init_data));
		mInitDialog.setMax(70);
		mInitDialog.setCancelable(false);
		//插入数据
		PrepareData prepare = new PrepareData(this);
		if (!prepare.isAlreadyInitData()) {
			mInitDialog.show();
			new InitDataAsyncTask().execute(prepare);
		}

		/* 抽屉导航相关 */
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.draw, GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK);

		mDrawerList.setAdapter(new SimpleAdapter(this, getDrawerItems(false),
				R.layout.drawer_item, new String[] { "drawer_img",
						"drawer_title" }, new int[] { R.id.drawer_img,
						R.id.drawer_title }));

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.draw,  /* nav drawer image to replace 'Up' caret */
                R.string.search_label,  /* "open drawer" description for accessibility */
                R.string.defalut_text  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle("onDrawerClosed");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("onDrawerOpened");
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        /** action_bar添加自定义view */
		View actionbarLayout = LayoutInflater.from(this).inflate(R.layout.action_bar, null);
		mActionbarSpinne = (Spinner) actionbarLayout.findViewById(R.id.action_bar_spinner);
		//setAdapter可以根据具体情况, 此处不必深究.
		mActionbarSpinne.setAdapter(new SimpleAdapter(this,
				getDrawerItems(true), R.layout.drawer_item, new String[] {
						"drawer_img", "drawer_title" }, new int[] {
						R.id.drawer_img, R.id.drawer_title }));
		mActionbarSpinne.setOnItemSelectedListener(new SpinnerItemSelectedListener());
		//使自定义的普通View能在title栏显示, actionBar.setCustomView能起作用.
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setCustomView(actionbarLayout);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
        if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // handles a click on a search suggestion; launches activity to show word
            Intent wordIntent = new Intent();
            wordIntent.setData(intent.getData());
            Cursor searchCursor = getContentResolver().query(intent.getData(), null, null, null, null);
            if (searchCursor != null && searchCursor.moveToFirst()) {
            	cityInput.setText(searchCursor.getString(searchCursor.getColumnIndex(City.CITY_DESCRIBE)));
			}
//            startActivity(wordIntent);

        } else if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // handles a search query
            String query = intent.getStringExtra(SearchManager.QUERY);
//            showResults(query);
        }
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_weather, menu);

		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		 switch (item.getItemId()) {
	        case android.R.id.home:
	        	this.finish();
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_search:
			if (HttpUtil.hasNetWork(this)) {
				dialog = ProgressDialog.show(this, "请稍等", "正在解析最新数据");
				new Thread() {
					@Override
					public void run() {
						Message message = handler.obtainMessage();
						message.what = MESSAGE_PARSE_DATA;
						// 查数据库,拼出http查询地址
						String cityName = cityInput.getText().toString();
						Cursor cursor = getContentResolver().query(
								City.CONTENT_URI, null, City.CITY_DESCRIBE + " = ?",
								new String[] { cityName }, null);

						if (cursor.moveToFirst()) {
							String cityId = cursor.getString(cursor
									.getColumnIndex("city_id"));
							String url = "http://www.weather.com.cn/data/cityinfo/"
									+ cityId + ".html";
							String weatherData = HttpUtil.getWeatherData(url);
							if (weatherData != null && !weatherData.equals("")) {
								City city = JsonUtil.parseJson(weatherData);
								if (city != null) {
									message.obj = city;
								}
							} else {
								Bundle bundle = new Bundle();
								bundle.putString("errorMessage", "网络不稳定");
								message.setData(bundle);
							}
						} else {
							Bundle bundle = new Bundle();
							bundle.putString("errorMessage", "支持70个大中城市");
							message.setData(bundle);
						}
						handler.sendMessage(message);
					}
				}.start();

			} else {
				Bundle bundle = new Bundle();
				bundle.putString("errorMessage", "网络无连接");
				handler.obtainMessage().setData(bundle);
			}
			break;
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case MESSAGE_PARSE_DATA:
				Bundle bundle = msg.getData();
				if (bundle.get("errorMessage") == null) {
					City city = (City) msg.obj;
					title.setText(city.getCityName() + "\n" + city.getLowTemp()
							+ " - " + city.getHighTemp() + "\n" + city.getWeather()
							+ "\n" + "更新时间 " + city.getpTime());
				} else {
					title.setText(bundle.get("errorMessage").toString());
				}
				dialog.dismiss();
				break;
			case MESSAGE_DRAWER_ITEM:
				List<Map<String, Object>> drawerItems =	(List<Map<String, Object>>) msg.obj;
				mDrawerList.setAdapter(new SimpleAdapter(WeatherActivity.this, drawerItems,
        				R.layout.drawer_item, new String[] { "drawer_img",
        						"drawer_title" }, new int[] { R.id.drawer_img,
        						R.id.drawer_title }));
				break;
			case MESSAGE_SPINNER_ITEM:
				List<Map<String, Object>> spinnerItems =	(List<Map<String, Object>>) msg.obj;
				mActionbarSpinne.setAdapter(new SimpleAdapter(WeatherActivity.this, spinnerItems,
        				R.layout.drawer_item, new String[] { "drawer_img",
        						"drawer_title" }, new int[] { R.id.drawer_img,
        						R.id.drawer_title }));
				break;
			default:
				break;
			}
		}
	};

	private class InitDataAsyncTask extends AsyncTask<PrepareData, Integer, Boolean> {
		@Override
		protected Boolean doInBackground(PrepareData... params) {
			try {
				PrepareData prepare = params[0];
				ArrayList<ContentProviderOperation> op = new ArrayList<ContentProviderOperation>();
				ContentValues cv = new ContentValues();

				cv.put(City.CITY_ID, 101010100);
				cv.put(City.CITY_NAME, "北京");
				cv.put(City.CITY_NICKNAME, "bj");
				cv.put(City.CITY_DESCRIBE, "北京");
				cv.put(City.CITY_IMG, R.drawable.city_nomal);
				cv.put(City.CITY_IMG_2, R.drawable.test);
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(1);

				cv.put(City.CITY_ID, 101020100);
				cv.put(City.CITY_NAME, "上海");
				cv.put(City.CITY_NICKNAME, "sh");
				cv.put(City.CITY_DESCRIBE, "上海");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(2);

				cv.put(City.CITY_ID, 101030100);
				cv.put(City.CITY_NAME, "天津");
				cv.put(City.CITY_NICKNAME, "tj");
				cv.put(City.CITY_DESCRIBE, "天津");
				cv.put(City.CITY_IMG, R.drawable.city_tj);
				cv.put(City.CITY_IMG_2, R.drawable.lichen11);
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				cv.put(City.CITY_IMG, R.drawable.city_nomal);
				cv.put(City.CITY_IMG_2, R.drawable.test);
				publishProgress(3);

				cv.put(City.CITY_ID, 101090101);
				cv.put(City.CITY_NAME, "石家庄");
				cv.put(City.CITY_NICKNAME, "sjz");
				cv.put(City.CITY_DESCRIBE, "石家庄");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(4);


				cv.put(City.CITY_ID, 101080101);
				cv.put(City.CITY_NAME, "呼和浩特");
				cv.put(City.CITY_NICKNAME, "hhht");
				cv.put(City.CITY_DESCRIBE, "呼和浩特");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(5);

				cv.put(City.CITY_ID, 101100101);
				cv.put(City.CITY_NAME, "太原");
				cv.put(City.CITY_NICKNAME, "ty");
				cv.put(City.CITY_DESCRIBE, "太原");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(6);

				cv.put(City.CITY_ID, 101070101);
				cv.put(City.CITY_NAME, "沈阳");
				cv.put(City.CITY_NICKNAME, "sy");
				cv.put(City.CITY_DESCRIBE, "沈阳");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(7);

				cv.put(City.CITY_ID, 101070201);
				cv.put(City.CITY_NAME, "大连");
				cv.put(City.CITY_NICKNAME, "dl");
				cv.put(City.CITY_DESCRIBE, "大连");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(8);

				cv.put(City.CITY_ID, 101060101);
				cv.put(City.CITY_NAME, "长春");
				cv.put(City.CITY_NICKNAME, "cc");
				cv.put(City.CITY_DESCRIBE, "长春");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(9);

				cv.put(City.CITY_ID, 101050101);
				cv.put(City.CITY_NAME, "哈尔滨");
				cv.put(City.CITY_NICKNAME, "heb");
				cv.put(City.CITY_DESCRIBE, "哈尔滨");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(10);

				cv.put(City.CITY_ID, 101190101);
				cv.put(City.CITY_NAME, "南京");
				cv.put(City.CITY_NICKNAME, "nj");
				cv.put(City.CITY_DESCRIBE, "南京");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(11);

				cv.put(City.CITY_ID, 101210101);
				cv.put(City.CITY_NAME, "杭州");
				cv.put(City.CITY_NICKNAME, "hz");
				cv.put(City.CITY_DESCRIBE, "杭州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(12);

				cv.put(City.CITY_ID, 101210401);
				cv.put(City.CITY_NAME, "宁波");
				cv.put(City.CITY_NICKNAME, "nb");
				cv.put(City.CITY_DESCRIBE, "宁波");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(13);

				cv.put(City.CITY_ID, 101220101);
				cv.put(City.CITY_NAME, "合肥");
				cv.put(City.CITY_NICKNAME, "hf");
				cv.put(City.CITY_DESCRIBE, "合肥");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(14);

				cv.put(City.CITY_ID, 101230101);
				cv.put(City.CITY_NAME, "福州");
				cv.put(City.CITY_NICKNAME, "fz");
				cv.put(City.CITY_DESCRIBE, "福州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(15);

				cv.put(City.CITY_ID, 101230201);
				cv.put(City.CITY_NAME, "厦门");
				cv.put(City.CITY_NICKNAME, "xm");
				cv.put(City.CITY_DESCRIBE, "厦门");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(16);

				cv.put(City.CITY_ID, 101240101);
				cv.put(City.CITY_NAME, "南昌");
				cv.put(City.CITY_NICKNAME, "nc");
				cv.put(City.CITY_DESCRIBE, "南昌");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(17);

				cv.put(City.CITY_ID, 101120101);
				cv.put(City.CITY_NAME, "济南");
				cv.put(City.CITY_NICKNAME, "jn");
				cv.put(City.CITY_DESCRIBE, "济南");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(18);

				cv.put(City.CITY_ID, 101120201);
				cv.put(City.CITY_NAME, "青岛");
				cv.put(City.CITY_NICKNAME, "qd");
				cv.put(City.CITY_DESCRIBE, "青岛");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(19);

				cv.put(City.CITY_ID, 101180101);
				cv.put(City.CITY_NAME, "郑州");
				cv.put(City.CITY_NICKNAME, "zz");
				cv.put(City.CITY_DESCRIBE, "郑州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(20);

				cv.put(City.CITY_ID, 101200101);
				cv.put(City.CITY_NAME, "武汉");
				cv.put(City.CITY_NICKNAME, "wh");
				cv.put(City.CITY_DESCRIBE, "武汉");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(21);

				cv.put(City.CITY_ID, 101250101);
				cv.put(City.CITY_NAME, "长沙");
				cv.put(City.CITY_NICKNAME, "cs");
				cv.put(City.CITY_DESCRIBE, "长沙");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(22);

				cv.put(City.CITY_ID, 101280101);
				cv.put(City.CITY_NAME, "广州");
				cv.put(City.CITY_NICKNAME, "gz");
				cv.put(City.CITY_DESCRIBE, "广州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(23);

				cv.put(City.CITY_ID, 101280601);
				cv.put(City.CITY_NAME, "深圳");
				cv.put(City.CITY_NICKNAME, "sz");
				cv.put(City.CITY_DESCRIBE, "深圳");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(24);

				cv.put(City.CITY_ID, 101300101);
				cv.put(City.CITY_NAME, "南宁");
				cv.put(City.CITY_NICKNAME, "nn");
				cv.put(City.CITY_DESCRIBE, "南宁");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(25);

				cv.put(City.CITY_ID, 101310101);
				cv.put(City.CITY_NAME, "海口");
				cv.put(City.CITY_NICKNAME, "hk");
				cv.put(City.CITY_DESCRIBE, "海口");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(26);

				cv.put(City.CITY_ID, 101040100);
				cv.put(City.CITY_NAME, "重庆");
				cv.put(City.CITY_NICKNAME, "cq");
				cv.put(City.CITY_DESCRIBE, "重庆");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(27);

				cv.put(City.CITY_ID, 101270101);
				cv.put(City.CITY_NAME, "成都");
				cv.put(City.CITY_NICKNAME, "cd");
				cv.put(City.CITY_DESCRIBE, "成都");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(28);

				cv.put(City.CITY_ID, 101260101);
				cv.put(City.CITY_NAME, "贵阳");
				cv.put(City.CITY_NICKNAME, "gy");
				cv.put(City.CITY_DESCRIBE, "贵阳");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(29);

				cv.put(City.CITY_ID, 101290101);
				cv.put(City.CITY_NAME, "昆明");
				cv.put(City.CITY_NICKNAME, "km");
				cv.put(City.CITY_DESCRIBE, "昆明");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(30);

				cv.put(City.CITY_ID, 101110101);
				cv.put(City.CITY_NAME, "西安");
				cv.put(City.CITY_NICKNAME, "xa");
				cv.put(City.CITY_DESCRIBE, "西安");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(31);

				cv.put(City.CITY_ID, 101160101);
				cv.put(City.CITY_NAME, "兰州");
				cv.put(City.CITY_NICKNAME, "lz");
				cv.put(City.CITY_DESCRIBE, "兰州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(32);

				cv.put(City.CITY_ID, 101150101);
				cv.put(City.CITY_NAME, "西宁");
				cv.put(City.CITY_NICKNAME, "xn");
				cv.put(City.CITY_DESCRIBE, "西宁");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(33);

				cv.put(City.CITY_ID, 101170101);
				cv.put(City.CITY_NAME, "银川");
				cv.put(City.CITY_NICKNAME, "yc");
				cv.put(City.CITY_DESCRIBE, "银川");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(34);

				cv.put(City.CITY_ID, 101130101);
				cv.put(City.CITY_NAME, "乌鲁木齐");
				cv.put(City.CITY_NICKNAME, "wlmq");
				cv.put(City.CITY_DESCRIBE, "乌鲁木齐");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(35);

				cv.put(City.CITY_ID, 101090501);
				cv.put(City.CITY_NAME, "唐山");
				cv.put(City.CITY_NICKNAME, "ts");
				cv.put(City.CITY_DESCRIBE, "唐山");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(36);

				cv.put(City.CITY_ID, 101091101);
				cv.put(City.CITY_NAME, "秦皇岛");
				cv.put(City.CITY_NICKNAME, "qhd");
				cv.put(City.CITY_DESCRIBE, "秦皇岛");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(37);

				cv.put(City.CITY_ID, 101080201);
				cv.put(City.CITY_NAME, "包头");
				cv.put(City.CITY_NICKNAME, "bt");
				cv.put(City.CITY_DESCRIBE, "包头");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(38);

				cv.put(City.CITY_ID, 101070601);
				cv.put(City.CITY_NAME, "丹东");
				cv.put(City.CITY_NICKNAME, "dd");
				cv.put(City.CITY_DESCRIBE, "丹东");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(39);

				cv.put(City.CITY_ID, 101070701);
				cv.put(City.CITY_NAME, "锦州");
				cv.put(City.CITY_NICKNAME, "jz");
				cv.put(City.CITY_DESCRIBE, "锦州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(40);

				cv.put(City.CITY_ID, 101070701);
				cv.put(City.CITY_NAME, "吉林");
				cv.put(City.CITY_NICKNAME, "jl");
				cv.put(City.CITY_DESCRIBE, "吉林");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(41);

				cv.put(City.CITY_ID, 101050301);
				cv.put(City.CITY_NAME, "牡丹江");
				cv.put(City.CITY_NICKNAME, "mdj");
				cv.put(City.CITY_DESCRIBE, "牡丹江");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(42);

				cv.put(City.CITY_ID, 101190201);
				cv.put(City.CITY_NAME, "无锡");
				cv.put(City.CITY_NICKNAME, "wx");
				cv.put(City.CITY_DESCRIBE, "无锡");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(43);

				cv.put(City.CITY_ID, 101190601);
				cv.put(City.CITY_NAME, "扬州");
				cv.put(City.CITY_NICKNAME, "yz");
				cv.put(City.CITY_DESCRIBE, "扬州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(44);

				cv.put(City.CITY_ID, 101190801);
				cv.put(City.CITY_NAME, "徐州");
				cv.put(City.CITY_NICKNAME, "xz");
				cv.put(City.CITY_DESCRIBE, "徐州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(45);

				cv.put(City.CITY_ID, 101210701);
				cv.put(City.CITY_NAME, "温州");
				cv.put(City.CITY_NICKNAME, "wz");
				cv.put(City.CITY_DESCRIBE, "温州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(46);

				cv.put(City.CITY_ID, 101210901);
				cv.put(City.CITY_NAME, "金华");
				cv.put(City.CITY_NICKNAME, "jh");
				cv.put(City.CITY_DESCRIBE, "金华");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(47);

				cv.put(City.CITY_ID, 101220201);
				cv.put(City.CITY_NAME, "蚌埠");
				cv.put(City.CITY_NICKNAME, "bb");
				cv.put(City.CITY_DESCRIBE, "蚌埠");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(48);

				cv.put(City.CITY_ID, 101220601);
				cv.put(City.CITY_NAME, "安庆");
				cv.put(City.CITY_NICKNAME, "aq");
				cv.put(City.CITY_DESCRIBE, "安庆");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(49);

				cv.put(City.CITY_ID, 101230501);
				cv.put(City.CITY_NAME, "泉州");
				cv.put(City.CITY_NICKNAME, "qz");
				cv.put(City.CITY_DESCRIBE, "泉州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(50);

				cv.put(City.CITY_ID, 101240201);
				cv.put(City.CITY_NAME, "九江");
				cv.put(City.CITY_NICKNAME, "jj");
				cv.put(City.CITY_DESCRIBE, "九江");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(51);

				cv.put(City.CITY_ID, 101240701);
				cv.put(City.CITY_NAME, "赣州");
				cv.put(City.CITY_NICKNAME, "gz");
				cv.put(City.CITY_DESCRIBE, "赣州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(52);

				cv.put(City.CITY_ID, 101120501);
				cv.put(City.CITY_NAME, "烟台");
				cv.put(City.CITY_NICKNAME, "yt");
				cv.put(City.CITY_DESCRIBE, "烟台");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(53);

				cv.put(City.CITY_ID, 101120701);
				cv.put(City.CITY_NAME, "济宁");
				cv.put(City.CITY_NICKNAME, "jn");
				cv.put(City.CITY_DESCRIBE, "济宁");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(54);

				cv.put(City.CITY_ID, 101180901);
				cv.put(City.CITY_NAME, "洛阳");
				cv.put(City.CITY_NICKNAME, "ly");
				cv.put(City.CITY_DESCRIBE, "洛阳");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(55);

				cv.put(City.CITY_ID, 101180501);
				cv.put(City.CITY_NAME, "平顶山");
				cv.put(City.CITY_NICKNAME, "pds");
				cv.put(City.CITY_DESCRIBE, "平顶山");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(56);

				cv.put(City.CITY_ID, 101200901);
				cv.put(City.CITY_NAME, "宜昌");
				cv.put(City.CITY_NICKNAME, "yc");
				cv.put(City.CITY_DESCRIBE, "宜昌");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(57);

				cv.put(City.CITY_ID, 101200201);
				cv.put(City.CITY_NAME, "襄樊");
				cv.put(City.CITY_NICKNAME, "xf");
				cv.put(City.CITY_DESCRIBE, "襄樊");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(58);

				cv.put(City.CITY_ID, 101251001);
				cv.put(City.CITY_NAME, "岳阳");
				cv.put(City.CITY_NICKNAME, "yy");
				cv.put(City.CITY_DESCRIBE, "岳阳");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(59);

				cv.put(City.CITY_ID, 101250601);
				cv.put(City.CITY_NAME, "常德");
				cv.put(City.CITY_NICKNAME, "cd");
				cv.put(City.CITY_DESCRIBE, "常德");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(60);

				cv.put(City.CITY_ID, 101280301);
				cv.put(City.CITY_NAME, "惠州");
				cv.put(City.CITY_NICKNAME, "hz");
				cv.put(City.CITY_DESCRIBE, "惠州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(61);

				cv.put(City.CITY_ID, 101281001);
				cv.put(City.CITY_NAME, "湛江");
				cv.put(City.CITY_NICKNAME, "zj");
				cv.put(City.CITY_DESCRIBE, "湛江");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(62);

				cv.put(City.CITY_ID, 101280201);
				cv.put(City.CITY_NAME, "韶关");
				cv.put(City.CITY_NICKNAME, "sg");
				cv.put(City.CITY_DESCRIBE, "韶关");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(63);

				cv.put(City.CITY_ID, 101300501);
				cv.put(City.CITY_NAME, "桂林");
				cv.put(City.CITY_NICKNAME, "gl");
				cv.put(City.CITY_DESCRIBE, "桂林");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(64);

				cv.put(City.CITY_ID, 101301301);
				cv.put(City.CITY_NAME, "北海");
				cv.put(City.CITY_NICKNAME, "bh");
				cv.put(City.CITY_DESCRIBE, "北海");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(65);

				cv.put(City.CITY_ID, 101310201);
				cv.put(City.CITY_NAME, "三亚");
				cv.put(City.CITY_NICKNAME, "sy");
				cv.put(City.CITY_DESCRIBE, "三亚");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(66);

				cv.put(City.CITY_ID, 101271001);
				cv.put(City.CITY_NAME, "泸州");
				cv.put(City.CITY_NICKNAME, "lz");
				cv.put(City.CITY_DESCRIBE, "泸州");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(67);

				cv.put(City.CITY_ID, 101270501);
				cv.put(City.CITY_NAME, "南充");
				cv.put(City.CITY_NICKNAME, "nc");
				cv.put(City.CITY_DESCRIBE, "南充");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(68);

				cv.put(City.CITY_ID, 101260201);
				cv.put(City.CITY_NAME, "遵义");
				cv.put(City.CITY_NICKNAME, "zy");
				cv.put(City.CITY_DESCRIBE, "遵义");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());
				publishProgress(69);

				cv.put(City.CITY_ID, 101290201);
				cv.put(City.CITY_NAME, "大理");
				cv.put(City.CITY_NICKNAME, "dl");
				cv.put(City.CITY_DESCRIBE, "大理");
				op.add(ContentProviderOperation.newInsert(City.CONTENT_URI).withValues(cv).build());

				WeatherActivity.this.getContentResolver().applyBatch(CityContentProvider.AUTHORITY, op);
				publishProgress(70);
				return true;
			} catch (Exception e) {
				Log.e("CityDatabaseHelper", e.getMessage());
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (mInitDialog.isShowing()) {
				mInitDialog.dismiss();
			}
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			mInitDialog.setProgress(values[0]);
		}

	}


	/**
	 * 准备抽屉导航的Item数据
	 */
	public List<Map<String, Object>> getDrawerItems(boolean showAll) {
		List<Map<String, Object>> list = new LinkedList<Map<String,Object>>();
		Map<String, Object> map = new HashMap<String, Object>();
		if (!showAll) {
			map.put("drawer_img", R.drawable.bird);
			map.put("drawer_title", "70个大中城市");
			list.add(map);
		} else {
			map.put("drawer_img", R.drawable.bird);
			map.put("drawer_title", "70个大中城市");
			list.add(map);
			Cursor cursor = getContentResolver().query(City.CONTENT_URI, null, null, null, null);
			List<String> cityNameList = new ArrayList<String>();
			while (cursor.moveToNext()) {
				cityNameList.add(cursor.getString(cursor.getColumnIndex(City.CITY_NAME)));
			}
			for (String cityName : cityNameList) {
				map = new HashMap<String, Object>();
				map.put("drawer_img", R.drawable.twitter);
				map.put("drawer_title", cityName);
				list.add(map);
			}
		}
		return list;
	}

    /**
     * 监听抽屉导航item点击事件
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	selectItem(view, position, MESSAGE_PARSE_DATA);
        }
    }

    /**
     * 监听action_bar的spinner item点击事件
     */
    private class SpinnerItemSelectedListener implements OnItemSelectedListener {
		@Override
		public void onItemSelected(AdapterView<?> arg0, View view, int position,
				long arg3) {
			selectItem(view, position, MESSAGE_SPINNER_ITEM);
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
    }

    private boolean flag = false;
    private void selectItem(View view, int position, int messageId) {
    	if (messageId == MESSAGE_PARSE_DATA) {
    		if (position == 0) {
        		new Thread(new Runnable() {
    				@Override
    				public void run() {
    					List<Map<String, Object>> drawerItems;
    					if (!flag) {
    						drawerItems = getDrawerItems(true);
    		    			flag = true;
    					} else {
    						drawerItems = getDrawerItems(false);
    						flag = false;
    					}
    					Message message = handler.obtainMessage(MESSAGE_DRAWER_ITEM);
    					message.obj = drawerItems;
    					handler.sendMessage(message);
    				}
    			}).start();
    		} else {
    			TextView title = (TextView) view.findViewById(R.id.drawer_title);
            	String cityName = title.getText().toString();
            	this.cityInput.setText(cityName);
    		}
		} else if (messageId == MESSAGE_SPINNER_ITEM) {
			if (position != 0) {
    			TextView title = (TextView) view.findViewById(R.id.drawer_title);
            	String cityName = title.getText().toString();
            	this.cityInput.setText(cityName);
    		}
		}
    }
}
