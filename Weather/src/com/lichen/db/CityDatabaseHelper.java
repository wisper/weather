package com.lichen.db;

import java.util.HashMap;

import com.lichen.weather.R;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.provider.BaseColumns;

public class CityDatabaseHelper extends SQLiteOpenHelper {

	protected static final String DATABASE_NAME = "city.db";
	protected static final int DATABASE_VERSION = 6;
	public  String[] columns = new String[] {
			SearchManager.SUGGEST_COLUMN_TEXT_1,
			SearchManager.SUGGEST_COLUMN_TEXT_2,
			SearchManager.SUGGEST_COLUMN_ICON_1,
			SearchManager.SUGGEST_COLUMN_ICON_2,
			BaseColumns._ID,
			SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID}; 
	
	private static final HashMap<String,String> mColumnMap = buildColumnMap();
	
	public CityDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	private static HashMap<String,String> buildColumnMap() {
        HashMap<String,String> map = new HashMap<String,String>();
        map.put(SearchManager.SUGGEST_COLUMN_TEXT_1, City.CITY_DESCRIBE + " as "+SearchManager.SUGGEST_COLUMN_TEXT_1);
        map.put(SearchManager.SUGGEST_COLUMN_TEXT_2, City.CITY_NICKNAME + " as "+SearchManager.SUGGEST_COLUMN_TEXT_2);
        map.put(SearchManager.SUGGEST_COLUMN_ICON_1, City.CITY_IMG + " as "+SearchManager.SUGGEST_COLUMN_ICON_1);
        map.put(SearchManager.SUGGEST_COLUMN_ICON_2, City.CITY_IMG_2 + " as "+SearchManager.SUGGEST_COLUMN_ICON_2);
        map.put(BaseColumns._ID, "rowid AS " + BaseColumns._ID);
        map.put(SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, "rowid AS " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID);
        return map;
    }
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table "
				+ City.TABLE_NAME
				+ "(_id integer primary key autoincrement, city_id integer, city_name text, city_nickname text, city_describe text, city_img text, city_img_2 text)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("drop table if exists " + City.TABLE_NAME);
		onCreate(db);
	}
	
	public Cursor search(String keyWord){
		SQLiteQueryBuilder builder=new SQLiteQueryBuilder();
		builder.setTables(City.TABLE_NAME);
		builder.setProjectionMap(mColumnMap);	
		SQLiteDatabase db=getReadableDatabase();
		
		return builder.query(db, columns, City.CITY_NAME + " like ? " + " or " + City.CITY_NICKNAME +" like ? ", new String[]{"%"+keyWord+"%", "%"+keyWord+"%"}, null, null,null);
	}
	
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101010100, '北京', '北京');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101010100, 'bj', '北京');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101020100, '上海', '上海');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101020100, 'sh', '上海');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101030100, '天津', '天津');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101030100, 'tj', '天津');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101040100, '重庆', '重庆');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101040100, 'cq', '重庆');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101340102, '台北', '台北');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101340102, 'tb', '台北');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101231001, '钓鱼岛', '钓鱼岛');");
//		db.execSQL("insert into "+ City.TABLE_NAME + " (city_id , city_name, city_describe) values " + "( 101231001, 'dyd', '钓鱼岛');");
}
