package com.lichen.db;

import android.net.Uri;
import android.provider.BaseColumns;

public class City implements BaseColumns{
	
	private static City city = null;

	public static final Uri CONTENT_URI = Uri.parse("content://"+CityContentProvider.AUTHORITY+"/city");
	public static final String TABLE_NAME = "city";
	public static final String CITY_ID = "city_id";
	public static final String CITY_NAME = "city_name";
	public static final String CITY_NICKNAME = "city_nickname";
	public static final String CITY_DESCRIBE = "city_describe";
	public static final String CITY_IMG = "city_img";
	public static final String CITY_IMG_2 = "city_img_2";
	
	private long cityId;
	private String cityName;
	private String lowTemp;
	private String highTemp;
	private String weather;
	private String pTime;
	
	public static City getInstance() {
		if (city == null) {
			synchronized (City.class) {
				if (city == null) {
					city = new City();
				}
			}
		}
		return city;
	}
	
	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getLowTemp() {
		return lowTemp;
	}

	public void setLowTemp(String lowTemp) {
		this.lowTemp = lowTemp;
	}

	public String getHighTemp() {
		return highTemp;
	}

	public void setHighTemp(String highTemp) {
		this.highTemp = highTemp;
	}


	public String getWeather() {
		return weather;
	}

	public void setWeather(String weather) {
		this.weather = weather;
	}

	public String getpTime() {
		return pTime;
	}

	public void setpTime(String pTime) {
		this.pTime = pTime;
	}
}
